import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/ez.png')} />
                    </View>
                    <Text style={styles.name}>Reza Mahendra</Text>
                </View>
                <View style={styles.statsContainer}>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>24</Text>
                        <Text style={styles.statTitle}>Tanggal Lahir</Text>
                    </View>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>12</Text>
                        <Text style={styles.statTitle}>Bulan Lahir</Text>
                    </View>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>2000</Text>
                        <Text style={styles.statTitle}>Tahun Lahir</Text>
                    </View>
                </View>
                <View style={styles.status}>
                    <Text style={styles.Jurusan}>Jurusan : </Text>
                        <Text style ={styles.state}>    Teknik Informatika</Text>
                </View>
                <View style={styles.class}>
                    <Text style={styles.Kelas}>Kelas    : </Text>
                    <Text style ={styles.state}>   Pagi A</Text>
                </View>
                <View style={styles.instagram}>
                    <Text style={styles.ig}>Instagram :  </Text>
                    <Text style ={styles.state}>       ezamhndr_</Text>
                </View>
                <View style={styles.Youtube}>
                    <Text style={styles.yt}>Youtube :</Text>
                    <Text style ={styles.state}>    Dream Winners</Text>
                </View>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: 'olive',
        flex:1,
        width: 500
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 90,
        height: 90,
        borderRadius: 68
    },
    name: {
        color: 'black',
        marginTop: 24,
        fontSize: 20,
        fontWeight: 'bold'
    },
    statsContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        margin: 50,
        bottom: 30
    },
    stat: {
        alignItems: 'center',
        flex : 1,
    },
    statAmount:{
        color:'black',
        fontSize: 18,
        fontWeight: 'bold'
    },
    statTitle: {
        fontSize: 16,
        color: 'black',
        fontWeight: '300',
        marginTop: 4
    },
    status: {
        bottom: 40,
        left: 90
    },
    Jurusan:{
        justifyContent: 'flex-end',
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
    },
    class: {
        fontSize : 18,
        bottom:55,
        left: 95
    },
    Kelas:{
        fontWeight: 'bold',
        fontSize : 20
    },
    instagram: {
        bottom : 70,
        left : 80
    },
    ig: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    Youtube:{
        bottom: 80,
        left : 90
    },
    yt: {
        fontWeight :'bold',
        fontSize: 20
    },
    state : {
        fontSize : 20,
        left : 72,
        bottom :24
    }
});