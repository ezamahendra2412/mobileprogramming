import React, {Component} from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class Logo extends Component {
    render(){
        return(
            <View style = {styles.container}>
                    <Image styles = {styles.logo}
                    source = {require('./assets/ee.png')}/>
                    <Text style={styles.logoText}>Welcome To Application Eza</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
          flexGrow: 1,
          justifyContent: 'flex-end',
          alignItems: 'center'
        }, 
    logoText: {
        marginVertical: 15,
        fontSize: 20,
        color: 'rgba(255, 255, 255, 0.7)'
    },
    logo: {
        width : 10,
        bottom: 15
    }
});
